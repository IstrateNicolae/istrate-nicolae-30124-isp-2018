package g30124.istrate.nicolae.l9.e2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.*;
import java.util.*;
 
public class ButtonCounter extends JFrame{
 
      HashMap accounts = new HashMap();
 
      JLabel tArea;
      JTextField tcounter;
      JTextArea counter;
      JButton increment;
 
      ButtonCounter(){

            setTitle("Incrementare");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            init();
            setSize(200,300);
            setVisible(true);
      }
 
      public void init(){
 
            this.setLayout(null);
            int width=80;int height = 20;

            tcounter = new JTextField();
            tcounter.setBounds(50,100,width, height);

           increment = new JButton("Click");
            increment.setBounds(50,50,width, height);
 
            increment.addActionListener(new Increment());
 
            add(tcounter);add(increment);
 
      }
 
      public static void main(String[] args) {
            new ButtonCounter();
      }
 
      class Increment implements ActionListener{
    	  
           int counter=1;
           
           public void actionPerformed(ActionEvent e){
        	   
        	   ButtonCounter.this.tcounter.setText("           "+counter);
        	   counter ++;
        	   
           }
      }
            
      }
