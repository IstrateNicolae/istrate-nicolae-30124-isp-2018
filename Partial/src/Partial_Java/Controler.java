package Partial_Java;

import java.util.ArrayList;
import java.util.Iterator;

public class Controler implements ControlerParcare{
   private ArrayList<Masina> parcare= new ArrayList<>(40);

    public void inregistreazaNumarInmatriculare(String numarInmatriculare)
    {
        Masina m=new Autoturism (numarInmatriculare);
        parcare.add(m);
        System.out.println("Masina s-a inregistrat !");
    }
    public boolean parcheazaMasina(Masina m)
    {
    	for(int i=0;i<parcare.size();i++)
    	{
    		if(parcare.get(i).equals(m))
    		{
    			return true;
    		}
    	}
        return false;
    }

    public Masina elibereazaLocaParcare(String numarInmatriculare)
    {
    	for(int i=0;i<parcare.size();i++)
    	{
    		if(numarInmatriculare.equals(parcare.get(i).getNumarInmatriculare()))
    		{
    			parcare.remove(i);
    			
    		}
    	}
    	System.out.println("Masina a fost stearsa !");
    	return null;
      
    }

    public void afiseazaStareParcare()
    {
    	int n=40;

        for(int i=0;i<parcare.size();i++)
    	{
    		n-=parcare.get(i).getDimensiune();
    	}

        System.out.println("In parcare sunt :"+n+" locuri libere");

    }
    
}
