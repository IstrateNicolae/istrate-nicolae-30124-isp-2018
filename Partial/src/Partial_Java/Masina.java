package Partial_Java;

import java.awt.*;

public abstract class Masina {
    private String numarInmatriculare;

    public Masina(String numarInmatriculare) {
        this.numarInmatriculare = numarInmatriculare;
    }

    public String getNumarInmatriculare() {
        return numarInmatriculare;
    }

    public abstract int getDimensiune();
}