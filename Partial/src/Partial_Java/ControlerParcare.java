package Partial_Java;

public interface ControlerParcare {



    public void inregistreazaNumarInmatriculare(String numarInmatriculare);


    public boolean parcheazaMasina(Masina m);


    public Masina elibereazaLocaParcare(String numarInmatriculare);


    public void afiseazaStareParcare();

}