package Partial_Java;

public class Main {

	public static void main(String[] args) {
		Masina m1=new Autoturism("1");
		Masina m2=new Autoturism("2");

		Controler c=new Controler();
		c.inregistreazaNumarInmatriculare(m1.getNumarInmatriculare());
		c.inregistreazaNumarInmatriculare(m2.getNumarInmatriculare());
		c.afiseazaStareParcare();
		c.elibereazaLocaParcare("1");
		c.afiseazaStareParcare();
		Masina m3=new Autoturism("3");
		System.out.println(" "+c.parcheazaMasina(m3));
		System.out.println(m1.getNumarInmatriculare());
		System.out.println(m1.getDimensiune());
		DrawingBoard b=new DrawingBoard();
        b.adauga(m1);
        b.adauga(m2);
        b.adauga(m3);
		
	}

}
