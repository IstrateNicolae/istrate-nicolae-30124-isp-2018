package Partial_Java;



import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard extends JFrame{
    ArrayList<Masina> masina = new ArrayList<>();


    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(400, 400);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
       
    }

    public void adauga(Masina m) {

        masina.add(m);
        this.repaint();
        
    }

    private void drawString(Graphics g, String text, int x, int y) {
        for (String line : text.split("\n"))
        {
        	g.drawString(line, x, y += g.getFontMetrics().getHeight());
        }
            
    }
    public void paint(Graphics g) {
         int x=250;
         
//        this.setFont(new Font("Tahoma",Font.BOLD,15));
        for(int i=0;i<masina.size();i++)
    	{
        	
        	drawString(g, masina.get(i).getNumarInmatriculare(),x, 250);
        	x+=20;
        	System.out.println(masina.get(i).getNumarInmatriculare());
    	}
    	
       

    }

}
