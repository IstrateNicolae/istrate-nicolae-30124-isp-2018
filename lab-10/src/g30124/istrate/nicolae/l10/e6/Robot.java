package g30124.istrate.nicolae.l10.e6;

public class Robot extends Thread {
    String name;
    int x, y;
    boolean isActive;
    Robot[][] map;

    public Robot(String name, Robot[][] map) {
        this.name = name;
        x = (int) (Math.random() * 10);
        y = (int) (Math.random() * 10);
        map[this.x][this.y] = this;
        this.isActive = true;
    }

    public void localization(Robot[][] map) {
        map[this.x][this.y] = this;
    }

    public void run() {
        System.out.println("Robotul " + name + " x:" + x + " y:" + y + " se misca");
        try {
            while (this.isActive == true) {
                int dir = (int) (Math.random() * 100) % 4;
                switch (dir) {
                    case 0:
                        if (x < 8)
                            if (map[this.x + 1][this.y] == null) {
                                //map[this.x][this.y]=null;
                                this.x = x + 1;
                                //localization(map);
                            }
                        System.out.println("Robotul " + name + " x:" + x + " y:" + y);
                        break;
                    case 1:
                        if (y < 8)
                            if (map[this.x][this.y+1] == null) {
                                //map[this.x][this.y]=null;
                                this.y = y + 1;
                                //localization(map);
                            }
                        System.out.println("Robotul " + name + " x:" + x + " y:" + y);
                        break;
                    case 2:
                        if (x > 1)
                            if (map[this.x-1][this.y] == null) {
                                //map[this.x][this.y]=null;
                                this.x = x - 1;
                                //localization(map);
                            }
                        System.out.println("Robotul " + name + " x:" + x + " y:" + y);
                        break;
                    case 3:
                        if (y > 1)
                            if (map[this.x][this.y-1] == null) {
                                //map[this.x][this.y]=null;
                                this.y = y - 1;
                                //localization(map);
                            }
                        System.out.println("Robotul " + name + " x:" + x + " y:" + y);
                        break;

                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Robot[][] map = new Robot[10][10];

        Robot r1 = new Robot("R2D2", map);
        r1.start();
        Robot r2 = new Robot("R2D3", map);
        r2.start();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++)
                System.out.print(map[i][j] + " ");
            System.out.println();
        }

    }
}
