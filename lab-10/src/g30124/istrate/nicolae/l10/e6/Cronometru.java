package g30124.istrate.nicolae.l10.e6;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Cronometru extends JFrame {
   JButton startpause;
   JButton reset;
   JLabel label;
  public  boolean active;
  public int timerValue=0;
    CronometruThread cronometruThread;
    public void setTimerValue(int timerValue) {
        this.timerValue = timerValue;
    }

    public int getTimerValue() {
        return timerValue;
    }


    @Override
    public boolean isActive() {
        return active;
    }

    public void MainFrame()
    {
        setTitle("CRONOMETRU");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(300,300);
        setVisible(true);
    }
    public void init(){
       active=false;
        startpause=new JButton("Start/Pause");
        startpause.setBounds(40,40,150,20);
        reset=new JButton("RESET");
        reset.setBounds(100,150,90,30);
        label=new JLabel("0");
        label.setBounds(50,100,15,15);
        startpause.addActionListener(new ApasareButon());
        reset.addActionListener(new ApasareReset());
        add(startpause);
        add(reset);
        add(label);
    }
    class ApasareButon implements ActionListener{
        public void actionPerformed(ActionEvent e)
        {
            cronometruThread.start();
            String c=String.valueOf(timerValue);
            label.setText(c);

        }
    }

    class ApasareReset implements ActionListener{
        public void actionPerformed(ActionEvent e){
            int nr=0;
            String c=String.valueOf(nr);
            label.setText(c);
            active=false;
        }
    }
    public static void main(String[] args) {
        Cronometru c=new Cronometru();
        c.MainFrame();
        c.init();

    }
}