package g30124.istrate.nicolae.l5.e4;

import g30124.istrate.nicolae.l5.e3.Controller;
import g30124.istrate.nicolae.l5.e3.LightSensor;
import g30124.istrate.nicolae.l5.e3.TemperatureSensor;

import java.util.Timer;
import java.util.TimerTask;

public class SingletonController {
    private static volatile SingletonController instance=null;
    private SingletonController() {}
    public static SingletonController getInstance() {
        synchronized (SingletonController.class)
        {
            if(instance==null)
                instance=new SingletonController();
        } return instance;
    }
    Timer timer=new Timer();
    LightSensor lightSensor=new LightSensor();
    TemperatureSensor temperatureSensor=new TemperatureSensor();
    private int secondsPassed=0;

    TimerTask task=new TimerTask() {
        @Override
        public void run() {
            secondsPassed++;
            if(secondsPassed>=20)
                timer.cancel();
            System.out.println(secondsPassed + ")" + "\n" +
                    "Temperature: " + temperatureSensor.readValue() + " Light: " + lightSensor.readValue());

        }
    };
    public void Control() {

        timer.scheduleAtFixedRate(task, 1000, 1000);
        {

        }
    }

    public static void main(String[] args) {
        SingletonController controller= SingletonController.getInstance();
        controller.Control();
    }
}