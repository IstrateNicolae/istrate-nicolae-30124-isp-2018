package g30124.istrate.nicolae.l5.e1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestClass {

	@Test
	  public void testGetAreaRect(){
        Shape rectangle = new Rectangle(2,4);
        rectangle.setColor("red");
        assertEquals(8, rectangle.getArea(),0.01);


    }
	
	@Test
    public void testGetAreaSquare(){
        Shape s = new Square() {
            @Override
            public double getSide(double side) {
                return super.getSide(side);
            }
        };
     ((Square)s).setSide(12);
        assertEquals(144.0, ((Square)s).getArea(),0.1);


	}

}
