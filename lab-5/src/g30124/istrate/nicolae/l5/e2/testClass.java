package g30124.istrate.nicolae.l5.e2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testClass {

	@Test
    public static void main(String[] args) {
        ProxyImage proxyImage=new ProxyImage("pic.jpg",false);
        ProxyImage proxyImage1=new ProxyImage("picture2.png",true);
        proxyImage.display();
        System.out.println("----");
        proxyImage1.display();
    }

}
