package g30124.istrate.nicolae.l3.ex4;

import becker.robots.*;


public class ex4 {
	public static void main(String[] args)
	   {
		// Set up the initial situation
		 City Sibiu = new City();
         Wall blockAve0 = new Wall(Sibiu, 1, 2, Direction.WEST);
         Wall blockAve1 = new Wall(Sibiu, 2, 2, Direction.WEST);
         Wall blockAve2 = new Wall(Sibiu, 1, 2, Direction.NORTH);
         Wall blockAve3 = new Wall(Sibiu, 2, 2, Direction.SOUTH);
         Wall blockAve4 = new Wall(Sibiu, 2, 3, Direction.SOUTH);
         Wall blockAve5 = new Wall(Sibiu, 2, 3, Direction.SOUTH);
         Wall blockAve6 = new Wall(Sibiu, 2, 3, Direction.EAST);
         Wall blockAve7 = new Wall(Sibiu, 1, 3, Direction.EAST);
         Wall blockAve8 = new Wall(Sibiu, 1, 3, Direction.NORTH);
         Robot karel = new Robot(Sibiu, 0, 3, Direction.EAST);
         
         while(true){
	     karel.move();
	     karel.turnLeft();
	     karel.turnLeft();
	     karel.turnLeft();
	     karel.move();
	     karel.move();
	     karel.move();
	     karel.turnLeft();
	     karel.turnLeft();
	     karel.turnLeft();
	     karel.move();
	     karel.move();
	     karel.move();
	     karel.turnLeft();
	     karel.turnLeft();
	     karel.turnLeft();
	     karel.move();
	     karel.move();
	     karel.move();
	     karel.turnLeft();
	     karel.turnLeft();
	     karel.turnLeft();
	     karel.move();
	     karel.move();
         }
	     
	   }
	
	
   

}