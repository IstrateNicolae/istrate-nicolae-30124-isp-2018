package g30124.istrate.nicolae.l3.ex3;

import becker.robots.*;

public class ex3 {
	public static void main(String[] args)
	   { 
		  City Medias = new City();
	      Robot karel = new Robot(Medias, 1, 1, Direction.NORTH);
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.turnLeft();	
	      karel.turnLeft();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	      karel.move();
	   }

}
