package g30124.istrate.nicolae.l8.e2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	    public static void main(String[] args) throws IOException {
	        System.out.println("Introduceti litera :");
	        char litera=(char) System.in.read();
	        File file=new File("data.txt");
	        BufferedReader bufferedreader=new BufferedReader(new FileReader(file));
	        String line;
	        int numar=0;
	        while((line=bufferedreader.readLine())!=null)
	        {
	            for(int i=0;i<line.length();i++)
	                if(line.charAt(i)==litera) {
	                	numar++;
	                }
	                    
	        }
	        System.out.println("Avem "+numar+" aparitii de "+litera);

	    }
	}
