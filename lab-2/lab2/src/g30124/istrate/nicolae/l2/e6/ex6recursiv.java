package g30124.istrate.nicolae.l2.e6;

import java.util.Scanner;

public class ex6recursiv {
	 public static void main(String[] args) {
	        int n,fact;
	        Scanner input=new Scanner(System.in);

	        n=input.nextInt();
	        fact=factorial(n);
	        System.out.println("Factorial method "+n+"!="+fact);
	    }

	    static int factorial(int nr)
	    {
	        if (nr==1)
	            return 1;
	        else return nr*factorial(nr-1);
	    }
		}