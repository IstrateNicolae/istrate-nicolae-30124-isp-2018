package g30124.istrate.nicolae.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class ex7 {
	 public static void main(String[] args)
	 {
		 Scanner in = new Scanner(System.in);
		 int number,guess,lives=0;
		 Random randomGenerator = new Random();
		 while(lives<3)
		 {
			 
			 number=randomGenerator.nextInt(100);
			 System.out.println(number); 
			 guess= in.nextInt();
			 if(guess==number)
			 {
				 System.out.println("You win!");
				 break;
			 }
			 if(guess<number)
			 {
				 System.out.println("Wrong answer, your number is too low!");
				 lives++;
			 }
			 else
			 {
				 System.out.println("Wrong answer, your number is too high!");
				 lives++;
			 }
			 if(lives==3)
			 {
				 System.out.println("You lose!");
			 }
		 }
		 }
}
		   
