package g30124.istrate.nicolae.l7.e1;

import java.util.Comparator;

public class BankAccount implements Comparable<BankAccount>{
	
	public static final Comparator BankAccountComparator = null;
	private String owner;
	private double balance;
	
	private void withdraw(double amount) {
		if(balance-amount<0) {
			System.out.println("Nu se poate realiza tranzactia!");
		}else
		{
			balance-=amount;
		}
		
	}
	private void deposit(double amount) {
		
		balance+=amount;
	}
	public String getOwner() {
		return owner;
	}
	public double getBalance() {
		return balance;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public BankAccount(String owner, double balance) {
		super();
		this.owner = owner;
		this.balance = balance;
	}

	@Override
	public boolean equals(Object o) {
		 if(o==null||!(o instanceof BankAccount) )
	            return false;
	        BankAccount x = (BankAccount)o;
	        return x.balance==balance&&x.owner.equals(owner);
	}
	@Override 
	public int hashCode(){
		return owner.hashCode();
		
	}
	
	
	public static void main(String[] args) {
		BankAccount a1=new BankAccount("Ana",100);
		BankAccount a2=new BankAccount("Ana",10000);
		
		if(a1.equals(a2))
			System.out.println(a1+" and "+a2+ " sunt egale!");
		else
			System.out.println(a1+" and "+a2+ " nu sunt egale !");
 
		
		
		
		
		

	}
	@Override
	public int compareTo(BankAccount arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
		

}
