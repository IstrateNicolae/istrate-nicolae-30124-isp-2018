package g30124.istrate.nicolae.l7.e4;

import java.util.HashMap;

public class Dictionary {
	HashMap dictionar=new HashMap();
	public void addWord(Word word,Definition definition)
    {

        dictionar.put(word,definition);
    }

    public Definition getDefinition(Word word)
    {
        return (Definition) dictionar.get(word);
    }

    public void getAllWords()
    {

        dictionar.forEach((key,value)-> System.out.println(key));
    }
    public void getAllDefinitions()
    {
        dictionar.forEach((key, value) -> System.out.println( value));
    }


    @Override
    public String toString() {
        return "Dictionary{" +
                "dictionar=" + dictionar +
                '}';
    
	
    }
}
