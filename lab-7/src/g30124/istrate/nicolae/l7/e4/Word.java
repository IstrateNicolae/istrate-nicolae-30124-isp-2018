package g30124.istrate.nicolae.l7.e4;

import java.util.Objects;

public class Word {
	private String nume;

	public Word(String nume) {
		super();
		this.nume = nume;
	}

	@Override
	public String toString() {
		return "Word =" + nume;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equals(nume, word.nume);
    }
	
	 @Override
	    public int hashCode() {

	        return Objects.hash(nume);
	    }
	

}
