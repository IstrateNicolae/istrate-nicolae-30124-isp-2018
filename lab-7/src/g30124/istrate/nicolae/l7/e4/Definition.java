package g30124.istrate.nicolae.l7.e4;

public class Definition {
	private String description;

	public Definition(String description) {
		super();
		this.description = description;
	}
	
	@Override
    public String toString() {
        return " Definition:"+ description;
    }

}
