package g30124.istrate.nicolae.l7.e2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;

import g30124.istrate.nicolae.l7.e1.BankAccount;

public class Main {
    public static void main(String[] args) {
       Bank bank=new Bank();

       bank.addAccount("C1",2002);
       bank.addAccount("C2",1201);
       bank.addAccount("C3",40);
//        bank.printAccounts();
//        System.out.println("-----");
//        bank.printAccounts(20,300);
       System.out.println(bank.getAccount("Sergiu").getBalance()); 

        ArrayList<BankAccount> bankAccounts= bank.getAccounts();

        Collections.sort(bankAccounts,BankAccount.BankAccountComparator);
        for(BankAccount b:bankAccounts)
            System.out.println(b.toString());

    }
}