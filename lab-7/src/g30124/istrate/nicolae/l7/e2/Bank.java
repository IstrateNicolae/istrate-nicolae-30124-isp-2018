package g30124.istrate.nicolae.l7.e2;

import g30124.istrate.nicolae.l7.e1.BankAccount;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class Bank   {
    ArrayList<BankAccount> accounts=new ArrayList<>();
    public void addAccount(String owner,double balance)
    {
        BankAccount b=new BankAccount(owner,balance);
        accounts.add(b);
        }
        public void printAccounts()
        {   sortList();
            for(BankAccount b:accounts)
                System.out.println(b.toString());
        }
        public void sortList()
        {
            Collections.sort(accounts);
        }
        public void printAccounts(double minBalance,double maxBalance)
        {   sortList();
                for(BankAccount b:accounts)
                {
                    if(b.getBalance() > minBalance && b.getBalance() < maxBalance)
                        System.out.println(b.toString());
                }
        }
        public BankAccount getAccount(String owner)
        {

            for(int i=0;i<accounts.size();i++)
        	{
        		if(owner.equals(accounts.get(i).getOwner()))
        		{
        			return accounts.get(i);
        			
        		}
        	}
            return null;
        }

    public ArrayList<BankAccount> getAccounts() {
        return accounts;
    }
}

