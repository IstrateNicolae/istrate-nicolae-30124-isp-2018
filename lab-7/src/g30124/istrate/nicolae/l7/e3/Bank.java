package g30124.istrate.nicolae.l7.e3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

import g30124.istrate.nicolae.l7.e1.BankAccount;

public class Bank {
    TreeSet<BankAccount> accounts =new TreeSet<>();
    public void addAccount(String owner,double balance)
    {
        BankAccount b=new BankAccount(owner,balance);
        accounts.add(b);
    }
    public void printAccounts()
    {
        for(BankAccount b:accounts)
            System.out.println(b.toString());
    }

    public void printAccounts(double minBalance,double maxBalance)
    {
        for(BankAccount b:accounts)
        {
            if(b.getBalance() > minBalance && b.getBalance() < maxBalance)
                System.out.println(b.toString());
        }
    }
    public BankAccount getAccount(String owner)
    {
        Iterator<BankAccount> iterator=accounts.iterator();
        while(iterator.hasNext())
        {
            BankAccount b= iterator.next();
            if(b.getOwner().equals(owner))
                return b;

        }
        return null;
    }

    public TreeSet<BankAccount> getAccounts() {
        return accounts;
    }
}