package g30124.istrate.nicolae.l4.e2;

public class MyPoint {
	private int x;
	private int y;
	
	//constructor with no arguments that sets x and y with 0
	public MyPoint() {
		this.x=0;
		this.y=0;
	}
	//constructor that sets x and y with variables given
	public MyPoint(int x,int y) {
		this.x=x;
		this.y=y;
	}
	//getters and setters
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	//setter fot both variables x and y
	public void setXY(int x,int y) {
		this.x=x;
		this.y=y;
	}
	//toString method
	public String toString()
	{
		return "( "+this.x+","+this.y+" )";
	}
	

	//distance method
	public int distance(MyPoint another)
	{
		   int dif1 = this.x - another.getX();
	       int dif2 = this.y - another.getY();
	        return (int)Math.sqrt((dif1 * dif1) +  (dif2 * dif2));
        
	}
	public int distance(int x,int y)
	{
		   int difx = this.x - x;
	       int dify = this.y - y;
	        return (int)Math.sqrt((difx * difx) +  (dify * dify));
        
	}
	
}


