package g30124.istrate.nicolae.l4.e3;

public class Circle {
	private double radius;
	private String color;
	
	
	
	public Circle()
	{
		this.radius=1.0;
		this.setColor("red");
	}
	
	public Circle(double radius,String color) {
		this.radius=radius;
		this.setColor(color);
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return 3.14*(radius*radius);
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
