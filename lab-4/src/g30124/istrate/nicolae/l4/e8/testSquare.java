package g30124.istrate.nicolae.l4.e8;

import static org.junit.Assert.*;

import org.junit.Test;

public class testSquare {

	@Test
	   public void shouldGetString(){
		 Square s=new Square(5,"red",true);    
		 assertEquals("A Square with side=5.0, which is a subclass of A Rectangle with width=5.0 and length=5.0 which,is a subclass of A Shape with color of red and filled.",s.toString());;
	    }

}
