package g30124.istrate.nicolae.l4.e8;

import static org.junit.Assert.*;

import org.junit.Test;

public class testCircle {

	 @Test
	    public void shouldGetArea(){
		 Circle c = new Circle(3.2);    
		 assertEquals(32.153,c.getArea(),0.1);;
	    }
	 

	 	 @Test
	 	    public void shouldGetPerimeter(){
	 		Circle c = new Circle(3.2);     
	 		 assertEquals(20.09,c.getPerimetre(),0.1);;
	 	    }
	 
	
	 	 @Test
	 	    public void shouldGetString(){
	 		Circle c = new Circle(3.2,"red",false);
	 		 assertEquals("A Circle with radius=3.2, which is a subclass of A Shape with color of red and not filled.",c.toString());
	 	    }
	 }



