package g30124.istrate.nicolae.l4.e8;
import g30124.istrate.nicolae.l4.e8.Shape;

public class Circle extends Shape {
	private double radius;
	
	public Circle() {
		this.radius=1.0;
	}
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public Circle(double radius)
	{
		this.radius=radius;
	}
	
	public Circle(double radius,String color,boolean filled) {
		super.color=color;
		super.filled=filled;
		this.radius=radius;
	}
	
	public double getArea() {
		return 3.14*(radius*radius);
	}
	
	public double getPerimetre() {
		return 2*3.14*radius;
	}
	
   public String toString() {
	   return "A Circle with radius="+this.radius+", which is a subclass of "+super.toString();
   }
	
	
}
