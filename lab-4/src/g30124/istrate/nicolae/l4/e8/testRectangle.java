package g30124.istrate.nicolae.l4.e8;

import static org.junit.Assert.*;

import org.junit.Test;

public class testRectangle {

	 @Test
	    public void shouldGetArea(){
		 Rectangle r1=new Rectangle(4,5);    
		 assertEquals(20,r1.getArea(),0.1);;
	    }
	 
	 @Test
	    public void shouldGetPerimeter(){
		 Rectangle r1=new Rectangle(4,5);    
		 assertEquals(18,r1.getPerimeter(),0.1);;
	    }
	 
	 @Test
	    public void shouldGetString(){
		 Rectangle r1=new Rectangle(4,5,"red",true);    
		 assertEquals("A Rectangle with width=4.0 and length=5.0 which,is a subclass of A Shape with color of red and filled.",r1.toString());;
	    }
}
