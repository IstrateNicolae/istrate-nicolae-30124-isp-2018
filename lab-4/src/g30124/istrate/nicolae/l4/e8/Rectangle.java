package g30124.istrate.nicolae.l4.e8;
import g30124.istrate.nicolae.l4.e8.Shape;

public class Rectangle extends Shape{
	protected double width;
	protected double length;

	//constructors
	public Rectangle() {
		this.width=1.0;
		this.length=1.0;
	}
	
public Rectangle(double width,double length) { 
	this.width=width;
	this.length=length;
		
	}
public Rectangle(double width,double length,String color,boolean filled) { 
	
	super.color=color;
	super.filled=filled;
	this.width=width;
	this.length=length;
		
	}
//getters and setters

public double getWidth() {
	return width;
}

public double getLength() {
	return length;
}

public void setWidth(double width) {
	this.width = width;
}

public void setLength(double length) {
	this.length = length;
}
//getArea and getPerimeter
public double getArea(){
	return length*width;
}

public double getPerimeter(){
	return 2*length + 2*width;
}
//toString
public String toString() {
	return "A Rectangle with width="+this.width+" and length="+this.length+" which,is a subclass of "+super.toString();
}

}
