package g30124.istrate.nicolae.l4.e5;
import g30124.istrate.nicolae.l4.e4.Author;

import static org.junit.Assert.*;

import org.junit.Test;

public class testBook {

	@Test
	public void test() {
		Author a = new Author("Dan","Dan@gmail.com",'m');
		Book b = new Book("Povesti",a, 30.0);    
	 assertEquals("Povesti by Dan(m) at Dan@gmail.com", b.toString());
	}

}
