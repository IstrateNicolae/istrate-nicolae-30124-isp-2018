package g30124.istrate.nicolae.l4.e6;
import g30124.istrate.nicolae.l4.e4.Author;

import static org.junit.Assert.*;

import org.junit.Test;

public class testBook {

	@Test
	public void testToString() {
		Author[] a = new Author[2];
		a[0]=new Author("Ion","@yahoo.com",'m');
				
		a[1]=new Author("Luca","Luca@yahoo.com",'m');
		Book b = new Book(" Nuvele ",a,10.0,3);
		
		assertEquals(" Nuvele  by 2 authors",b.toString());
		
	}
	
	@Test
	public void testPrintAuthors() {
		Author[] a = new Author[2];
		a[0]=new Author("Ion","Ion@yahoo.com",'m');
				
		a[1]=new Author("Ana","Ana@yahoo.com",'f');
		Book b1 = new Book("b1",a,50,23);
		b1.printAuthors();
		assertEquals("Ion",a[0].getName());
		assertEquals("Ana",a[1].getName());
		
		
	}

}
