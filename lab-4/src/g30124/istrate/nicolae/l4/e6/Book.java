package g30124.istrate.nicolae.l4.e6;
import g30124.istrate.nicolae.l4.e4.Author;

public class Book {
	
	private String name;
	private Author[] author;
	private double price;
	private int qtylnStock=0;
	
	//constructors
	public Book(String name, Author[] author, double price) {
		super();
		this.name = name;
		this.author = author;
		this.price = price;
	}

	public Book(String name, Author[] author, double price, int qtylnStock) {
		super();
		this.name = name;
		this.author = author;
		this.price = price;
		this.qtylnStock = qtylnStock;
	}
	//setters and getters
		public String getName() {
			return name;
		}
		public Author[] getAuthor() {
			return author;
		}
		public double getPrice() {
			return price;
		}
		public int getQtylnStock() {
			return qtylnStock;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public void setQtylnStock(int qtylnStock) {
			this.qtylnStock = qtylnStock;
		}
	//toString method
	public String toString () {
		return this.name+" by "+author.length+" authors";
		
	}
	void printAuthors(){
		for(Author a:author){
			System.out.println(a.getName());
			}
		}

	

}
