package g30124.istrate.nicolae.l4.e7;
import g30124.istrate.nicolae.l4.e7.Circle;

public class Cylinder extends Circle{
	
    private double height;
    
    public Cylinder(){
    	height=1.0;
	}
    
	public Cylinder(double radius){
		super(radius);
		
	}
	
	public Cylinder(double radius,double height){
		super(radius);
		this.height=height;
	}
	
	public double getHeight(){
		return this.height;
	}
	
	public double getArea(){
		return super.getArea();
	}
	
	public double getVolume(){
		return getArea()*height;
	}
   
}
