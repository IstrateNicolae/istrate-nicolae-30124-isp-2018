package g30124.istrate.nicolae.l6.e1;

import java.awt.*;


public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle("1",80,25,Color.RED, 90,false);
        b1.addShape(s1);
        Shape s2 = new Circle("2",1,100,Color.GREEN, 100,true);
        b1.addShape(s2);
        Shape r1=new Rectangle("3",100,100,Color.YELLOW,100,true);
        b1.addShape(r1);
       // b1.deleteById("3");
    }
}
