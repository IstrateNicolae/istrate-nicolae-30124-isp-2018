package g30124.istrate.nicolae.l6.e1;


import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(String id,int x,int y ,Color color, int length,boolean fill) {
        super(id,x,y,color,fill);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), length, length/2);
        if(isFill()==true) {
        	g.fillRect(getX(), getY(), length, length/2);
        }
    }
}