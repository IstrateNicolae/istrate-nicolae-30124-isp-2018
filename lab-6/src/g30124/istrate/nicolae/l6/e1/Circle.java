package g30124.istrate.nicolae.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;


    public Circle(String id,int x,int y,Color color, int radius,boolean fill) {
        super(id,x,y,color,fill);
        this.radius = radius;

    }
    

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(isFill()==true) {
        	g.fillOval(getX(),getY(),radius,radius);
        }
        
    }
}

