package g30124.istrate.nicolae.l6.e1;

import java.awt.Color;
import java.awt.Graphics;

public abstract class Shape {

    private Color color;
    private String id;
    private int x;
    private int y;
    boolean fill;
   

    public Shape(String id,int x,int y,Color color,boolean fill) {
        this.color = color;
        this.id=id;
        this.x=x;
        this.y=y;
        this.fill=fill;
        
    }
    public boolean isFill() {
		return fill;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}