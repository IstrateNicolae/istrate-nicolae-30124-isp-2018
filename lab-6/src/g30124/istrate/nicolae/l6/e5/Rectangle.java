package g30124.istrate.nicolae.l6.e5;


import java.awt.*;

public class Rectangle extends Shape{
	
    private int high;
    private int length;
    
    

    public Rectangle(String id, int x, int y, Color color, boolean fill, int high, int length) {
		super(id, x, y, Color.RED, true);
		this.high = high;
		this.length = length;
	}


	@Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), length, length/2);
        if(isFill()==true) {
        	g.fillRect(getX(), getY(), length, length/2);
        }
    }
}