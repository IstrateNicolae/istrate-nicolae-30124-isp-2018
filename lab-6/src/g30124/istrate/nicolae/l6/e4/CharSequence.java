package g30124.istrate.nicolae.l6.e4;

public class CharSequence implements java.lang.CharSequence {
	
	private char[] ch;

	public CharSequence(char[] ch) {
		this.ch=ch;
	}

	@Override
	public char charAt(int index) {
		return ch[index];
	}

	@Override
	public int length() {
		return ch.length;
	}

	@Override
	public java.lang.CharSequence subSequence(int star, int end) {
		return null;
	}
	

}