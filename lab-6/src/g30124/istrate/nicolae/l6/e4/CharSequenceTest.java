package g30124.istrate.nicolae.l6.e4;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CharSequenceTest {

	private CharSequence charSequence;

	    @Test
	    public void shouldGetChar()
	    {
	    	char[] sir= {'a','n','a'};
	    	charSequence=new CharSequence(sir);
			assertEquals(charSequence.charAt(1),'n');
			assertEquals(charSequence.length(),3);
	    }

}

