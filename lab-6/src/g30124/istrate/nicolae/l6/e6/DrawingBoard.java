package g30124.istrate.nicolae.l6.e6;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard extends JFrame {

   Fractal[] fractals=new Fractal[100];


    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(400,600);
        this.setVisible(true);

    }

    public void addFractal(Fractal fractals1){
        for(int i=0;i<fractals.length;i++){
            if(fractals[i]==null){
                fractals[i] = fractals1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<fractals.length;i++){
            if(fractals[i]!=null)
                fractals[i].draw(g,fractals[i].getX(),fractals[i].getY(),fractals[i].getRadius(),fractals[i].getRadius());
        }

    }
}
