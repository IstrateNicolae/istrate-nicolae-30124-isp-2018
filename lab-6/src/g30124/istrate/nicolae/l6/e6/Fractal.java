package g30124.istrate.nicolae.l6.e6;

import java.awt.*;

public class Fractal {
    private int x,y;
    float radius;
    private Graphics g;

    public Fractal(int x, int y, float radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public float getRadius() {
        return radius;
    }

    public void draw(Graphics g, int x, int y, float height, float width) {

    	g.drawOval(x, y,(int) radius, (int)radius);
    	  if(radius > 2) {
    	    radius*=0.90f;
    	    draw(g,x, y,radius, radius);
    	    
    	  }
    	  

    }
}