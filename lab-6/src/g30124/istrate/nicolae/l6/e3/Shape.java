package g30124.istrate.nicolae.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public  interface Shape {

    public boolean isFill() ;
	public String getId();
	public void setId(String id) ;
	public int getX();
	public int getY() ;
    public Color getColor() ;
    public void setColor(Color color) ;
    public abstract void draw(Graphics g);
}