package g30124.istrate.nicolae.l6.e3;

import java.awt.*;

public class Circle implements Shape{

    public Circle(int radius, Color color, String id, int x, int y, boolean fill) {
		super();
		this.radius = radius;
		this.color = color;
		this.id = id;
		this.x = x;
		this.y = y;
		this.fill = fill;
	}


	private int radius;
	private Color color;
    private String id;
    private int x;
    private int y;
    boolean fill;



    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(getX(),getY(),radius,radius);
        if(isFill()==true) {
        	g.fillOval(getX(),getY(),radius,radius);
        }
        
    }


	@Override
	public boolean isFill() {
		return fill;
	}


	@Override
	public String getId() {
		return id;
	}


	@Override
	public void setId(String id) {
		this.id = id;
		
	}


	@Override
	public int getX() {
		return x;
	}


	@Override
	public int getY() {
		return y;
	}


	@Override
	public Color getColor() {
		return color;
	}


	@Override
	public void setColor(Color color) {
		 this.color = color;
		
	}
    
}
