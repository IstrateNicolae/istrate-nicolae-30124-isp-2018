package g30124.istrate.nicolae.l6.e3;

import java.awt.*;


public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(90,Color.blue,"1",50,50,true);
        b1.addShape(s1);
        Shape s2 = new Circle(67,Color.GREEN,"1",100,100,true);
        b1.addShape(s2);
        Shape r1=new Rectangle(80,Color.GRAY,"3",100,200,true);
        b1.addShape(r1);
       // b1.deleteById("3");
    }
}
