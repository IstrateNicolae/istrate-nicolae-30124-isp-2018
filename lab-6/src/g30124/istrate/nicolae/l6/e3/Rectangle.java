package g30124.istrate.nicolae.l6.e3;


import java.awt.*;

public class Rectangle implements Shape{

    private int length;
	private Color color;
    private String id;
    private int x;
    private int y;
    boolean fill;


    public Rectangle(int length, Color color, String id, int x, int y, boolean fill) {
		super();
		this.length = length;
		this.color = color;
		this.id = id;
		this.x = x;
		this.y = y;
		this.fill = fill;
	}
	@Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(), getY(), length, length/2);
        if(isFill()==true) {
        	g.fillRect(getX(), getY(), length, length/2);
        }
    }
    @Override
	public boolean isFill() {
		return fill;
	}


	@Override
	public String getId() {
		return id;
	}


	@Override
	public void setId(String id) {
		this.id = id;
		
	}


	@Override
	public int getX() {
		return x;
	}


	@Override
	public int getY() {
		return y;
	}


	@Override
	public Color getColor() {
		return color;
	}


	@Override
	public void setColor(Color color) {
		 this.color = color;
		
	}
}